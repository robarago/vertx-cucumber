@echo off
echo ########################
echo # CT: Create Transaction
echo ########################
echo CT-A BEFORE TODAY: REF 1 - Positive amount
curl -s -X POST -H "Content-Type: application/json" -d "{\"account_iban\":\"ES1938491\",\"amount\":3435.5,\"fee\":1,\"date\":\"2019-07-16T16:55:42.000Z\"}" "localhost:3000/transaction" |jq ".reference!=null"
echo CT-B: AFTER TODAY REF 2
curl -s -X POST -H "Content-Type: application/json" -d "{\"account_iban\":\"ES1938491\",\"amount\":-34,\"fee\":0.5,\"date\":\"2021-05-17T14:25:43.400Z\"}" "localhost:3000/transaction" |jq ".reference!=null"
echo -s CT-C: EQUALS TODAY REF 3
curl -s -X POST -H "Content-Type: application/json" -d "{\"account_iban\":\"ES1938491\",\"amount\":50.0,\"fee\":10.0}" "localhost:3000/transaction" |jq ".reference!=null and .date!=null"
