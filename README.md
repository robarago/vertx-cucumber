# Code Challenge Solution - by Roberto Aragón

## Assumptions

- A "big application" is a complex, enterprise-ready one: I've assumed
  we're talking about a complex distributed system with single responsibility
	services dealing with more complex versions of the tiny cases exposed
	in this challenge.
- An small system can have a good architecture: I prefer many small replaceable
  modules (following the sense of Liskov Substitution Principle).
- The solution should be highly distributable and scalable.
- I've forced myself to KISS (the lack of time also helps)

## Framework choices

I've chosen vert.x over Spring Boot because I've used Spring Boot before and
wanted to test an alternative. Also, I liked the concept of the integration
of REST microservices with an event bus in the same framework. Interprocess
communication it's so important that I needed an straightforward approach,
and I should have liked to have time to test Kafka with vert.x!

I've used OpenAPI generator only to generate the model classes (`Transaction`,
`Status` and `Reference`). My experience with this tool is very good, but the
vert.x server generator is not good enough for this case. At least, the
OpenAPI3 validator helped a lot with param validations.

## Microservice patterns

I've tried to apply some microservice patterns as a exemplification of what
I like in a microservice architecture. I've made the `BalanceBackEnd` as an
aggregate, favoured the event sourcing by using the vert.x event bus, and
I've choosen the orchestration-based saga because I feel the balance service
is important enough to seek for stronger delivery warranties in that case.

## ATDD (my way)

ATDD tests are in the `atdd.md` file.

In addition to the business rules from A to H of the requirements for the
*Transaction status* operation, I've added two failure-checking cases for
this operation and more additional acceptance tests for the other two
operations.

Instead of using Cucumber or Rest Assured, I wanted to show how some e2e or
component tests can be done fast and simple using `curl` and `jq`. The
`seed` and `tests` files contains one test for every ATDD case in the 
`atdd.md` file (the test reference is shown after each test result).

## What I left undone

IBAN validation, dockerization, OpenAPI3 generator templates and/or a new
vert.x generator, Cucumber or Rest Assured integration, JUnit, etc.

Time is short!

## How to compile/run the code

### Software requisites

Please, install this tools to compile and run the project. This tools
are available for Mac OSX, GNU/Linux and Windows.

- JDK > 1.8
- Maven > 3.3.9
- curl > 7 (others might work)
- jq > 1.5 (others might work)

### Compiling and executing the microservice

```console
mvn compile exec:java
```

Wait for this traces to show up:

	(...)
	org.cochabaen.BalanceVerticle running...
	org.cochabaen.TransactionVerticle running...
	org.cochabaen.MicroServiceVerticle running...

Or, if you want to generate a fatjar:

```console
mvn package
```

That can be distributed and run with:

```console
java -jar code-challenge-backend-1.0-SNAPSHOT-fat.jar
```

## How to test

- Mac OSX or GNU/Linux

```console
$ . seed
$ . tests
```

- Windows:

```console
> seed
> tests
```

Many thanks for your time.
