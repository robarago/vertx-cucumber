package org.cochabaen.test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;

import org.javatuples.Pair;
import org.junit.Test;

public class MicroServiceTest {
	private static final String NOT_STORED = "not stored";
	private static final String STORED_SETTLED_REF = "12345A";
	private static final String STORED_PENDING_REF = "12346A";
	private static final String STORED_FUTURE_REF = "12347A";
	private static final String json = "{\"reference\":\"%s\",\"channel\":\"%s\"}";
	
	@SuppressWarnings("serial")
	private static Map<String, Pair<Number, Number>> seed = new HashMap<String, Pair<Number, Number>>() {{
		put(STORED_SETTLED_REF, new Pair<Number, Number>(3435.5, 1));
		put(STORED_PENDING_REF, new Pair<Number, Number>(50.0, 10.0));
		put(STORED_FUTURE_REF, new Pair<Number, Number>(-34, 0.5));
	}};
	
	/*
	 * A)
	 */
	@Test
	public void test_a_status_invalid_any() {
		given() // A transaction that is not stored in our system
			.contentType("application/json")
			.body(String.format(json, NOT_STORED, "ATM"))
		.when() // I check the status from any channel
			.get("/transaction/status")
		.then() // The system returns the status 'INVALID'
		.log().body()
			.body("status", is("INVALID"));
	}

	/*
	 * B)
	 */
	@Test
	public void test_b_status_settled_client_or_atm() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_SETTLED_REF, "ATM"))
		.when() // I check theº status from CLIENT or ATM channel
		        // And the transaction date is before today
			.get("/transaction/status")
		.then()
		.log().body()
			.body("status", is("SETTLED")) // The system returns the status 'SETTLED'
			.body("amount", is(seed.get(STORED_SETTLED_REF).getValue0().floatValue()
					- seed.get(STORED_SETTLED_REF).getValue1().floatValue())); // And the amount substracting the fee
	}
	
	/*
	 * C)
	 */
	@Test
	public void test_c_status_settled_internal() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_SETTLED_REF, "INTERNAL"))
		.when() // I check the status from INTERNAL channel
		        // And the transaction date is before today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("SETTLED")) // The system returns the status 'SETTLED'
			.body("amount", is(3435.5F))// And the amount
			.body("fee", is(1.0F));
	}

	/*
	 * D)
	 */
	@Test
	public void test_d_status_pending_client() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_PENDING_REF, "CLIENT"))
		.when() // I check the status from CLIENT or ATM channel
		        // And the transaction date is equals to today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("PENDING")) // The system returns the status 'PENDING'
			.body("amount", is(40.0F)); //And the amount substracting the fee
	}
	
	/*
	 * E)
	 */
	@Test
	public void test_e_status_pending_internal() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_PENDING_REF, "INTERNAL"))
		.when() // I check the status from INTERNAL channel
		        // And the transaction date is equals to today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("PENDING")) // The system returns the status 'PENDING'
			.body("amount", is(50.0F)) // And the amount
			.body("fee", is(10.0F)); //And the fee
	}
	
	/*
	 * F)
	 */
	@Test
	public void test_f_status_future_client() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_FUTURE_REF, "CLIENT"))
		.when() // I check the status from CLIENT channel
		        // And the transaction date is greater than today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("FUTURE")) // The system returns the status 'FUTURE'
			.body("amount", is(-34.5F)); // And the amount substracting the fee
	}
	
	/*
	 * G)
	 */
	@Test
	public void test_g_status_pending_atm() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_PENDING_REF, "ATM"))
		.when() // I check the status from ATM channel
		        // And the transaction date is greater than today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("PENDING")) // The system returns the status 'PENDING'
			.body("amount", is(40.0F)); // And the amount substracting the fee
	}

	/*
	 * H)
	 */
	@Test
	public void test_h_status_future_internal() {
		given() // A transaction that is stored in our system
			.contentType("application/json")
			.body(String.format(json, STORED_FUTURE_REF, "INTERNAL"))
		.when() // I check the status from INTERNAL channel
		        // And the transaction date is greater than today
			.get("/transaction/status")
		.then() 
		.log().body()
			.body("status", is("FUTURE")) // The system returns the status 'FUTURE'
			.body("amount", is(-34.0F)) //  And the amount
			.body("fee", is(0.5F)); // And the fee
	}
}
