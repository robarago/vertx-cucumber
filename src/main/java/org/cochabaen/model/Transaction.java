package org.cochabaen.model;

import java.util.Date;
import java.util.Objects;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Transaction as stored in the database
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {

	private String reference;
	private String account_iban;
	private Date date;
	private BigDecimal amount;
	private BigDecimal fee;
	private String description;

	public Transaction() {

	}

	public Transaction(String reference, String account_iban, Date date,
			BigDecimal amount, BigDecimal fee, String description) {
		this.reference = reference;
		this.account_iban = account_iban;
		this.date = date;
		this.amount = amount;
		this.fee = fee;
		this.description = description;
	}

	@JsonProperty("reference")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@JsonProperty("account_iban")
	public String getAccountIban() {
		return account_iban;
	}

	public void setAccountIban(String account_iban) {
		this.account_iban = account_iban;
	}

	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	public void setDate(Date date) {
		this.date = date;
	}

	@JsonProperty("amount")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@JsonProperty("fee")
	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Transaction transaction = (Transaction) o;
		return Objects.equals(reference, transaction.reference)
				&& Objects.equals(account_iban, transaction.account_iban)
				&& Objects.equals(date, transaction.date)
				&& Objects.equals(amount, transaction.amount)
				&& Objects.equals(fee, transaction.fee)
				&& Objects.equals(description, transaction.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(reference, account_iban, date, amount, fee,
				description);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Transaction {\n");

		sb.append("    reference: ").append(toIndentedString(reference))
				.append("\n");
		sb.append("    account_iban: ").append(toIndentedString(account_iban))
				.append("\n");
		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
		sb.append("    fee: ").append(toIndentedString(fee)).append("\n");
		sb.append("    description: ").append(toIndentedString(description))
				.append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
