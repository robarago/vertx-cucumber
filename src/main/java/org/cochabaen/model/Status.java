package org.cochabaen.model;

import java.math.BigDecimal;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Transaction status data
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Status {
	private String reference;

	public enum StatusEnum {
		PENDING("PENDING"), SETTLED("SETTLED"), FUTURE("FUTURE"), INVALID(
				"INVALID");

		private String value;

		StatusEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return value;
		}
	}

	private StatusEnum status;
	private BigDecimal amount;
	private BigDecimal fee;

	public Status() {
	}

	public Status(String reference, StatusEnum status, BigDecimal amount,
			BigDecimal fee) {
		this.reference = reference;
		this.status = status;
		this.amount = amount;
		this.fee = fee;
	}

	public Status(String ref, StatusEnum status) {
		this.reference = ref;
		this.status = status;
	}

	public Status(String ref, StatusEnum status, BigDecimal amount) {
		this.reference = ref;
		this.status = status;
		this.amount = amount;
	}

	@JsonProperty("reference")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@JsonProperty("status")
	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	@JsonProperty("amount")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@JsonProperty("fee")
	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Status status = (Status) o;
		return Objects.equals(reference, status.reference)
				&& Objects.equals(status, status.status)
				&& Objects.equals(amount, status.amount)
				&& Objects.equals(fee, status.fee);
	}

	@Override
	public int hashCode() {
		return Objects.hash(reference, status, amount, fee);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Status {\n");
		sb.append("    reference: ").append(toIndentedString(reference))
				.append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
		sb.append("    fee: ").append(toIndentedString(fee)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}

		return o.toString().replace("\n", "\n    ");
	}
}
