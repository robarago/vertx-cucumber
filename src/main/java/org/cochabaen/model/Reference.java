package org.cochabaen.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Transaction reference request
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reference {
	private String reference;

	public enum ChannelEnum {
		CLIENT("CLIENT"), ATM("ATM"), INTERNAL("INTERNAL");

		private String value;

		ChannelEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return value;
		}
	}

	private ChannelEnum channel;

	public Reference() {
	}

	public Reference(String reference, ChannelEnum channel) {
		this.reference = reference;
		this.channel = channel;
	}

	@JsonProperty("reference")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@JsonProperty("channel")
	public ChannelEnum getChannel() {
		return channel;
	}

	public void setChannel(ChannelEnum channel) {
		this.channel = channel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Reference reference = (Reference) o;
		return Objects.equals(reference, reference.reference)
				&& Objects.equals(channel, reference.channel);
	}

	@Override
	public int hashCode() {
		return Objects.hash(reference, channel);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Reference {\n");
		sb.append("    reference: ").append(toIndentedString(reference))
				.append("\n");
		sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}

		return o.toString().replace("\n", "\n    ");
	}
}
