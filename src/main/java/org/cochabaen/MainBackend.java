package org.cochabaen;

import io.vertx.core.Vertx;

public class MainBackend {
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new BalanceVerticle());
		vertx.deployVerticle(new TransactionVerticle());
		vertx.deployVerticle(new MicroServiceVerticle());
	}
}
