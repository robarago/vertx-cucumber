package org.cochabaen;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.cochabaen.model.Status;
import org.cochabaen.model.Status.StatusEnum;
import org.cochabaen.model.Transaction;

public class MemoryTransactionBackEnd {
	List<Transaction> transactions;

	MemoryTransactionBackEnd() {
		transactions = new ArrayList<>();
	}

	public int getTransactionCount() {
		return transactions.size();
	}

	public void createTransaction(Transaction t) {
		transactions.add(t);
	}

	public List<Transaction> findByIban(String iban, String order) {
		System.out.println("Source iban: " + iban);
		return transactions.stream().filter(t -> t.getAccountIban().equals(iban))
				.sorted((a, b) -> "desc".equals(order)
						? b.getAmount().compareTo(a.getAmount())
						: a.getAmount().compareTo(b.getAmount()))
				.collect(Collectors.toList());
	}

	public Status getTransactionStatus(String ref, String channel) {
		System.out.println("Ref: " + ref);
		System.out.println("channel: " + channel);
		Transaction found = checkTransactionExists(ref);
		if (found == null)
			return new Status(ref, StatusEnum.INVALID);
		else {
			Date today = truncate(new Date());
			Date tdate = truncate(found.getDate());
			BigDecimal amount = "INTERNAL".equals(channel) ? found.getAmount()
					: found.getAmount().subtract(found.getFee());
			BigDecimal fee = "INTERNAL".equals(channel) ? found.getFee() : null;
			if (today.equals(tdate))
				return new Status(found.getReference(), StatusEnum.PENDING, amount,
						fee);
			else if (today.after(found.getDate()))
				return new Status(found.getReference(), StatusEnum.SETTLED, amount,
						fee);
			else
				return new Status(found.getReference(), StatusEnum.FUTURE, amount, fee);
		}
	}

	public Transaction checkTransactionExists(String ref) {
		return transactions.stream().filter(t -> t.getReference().equals(ref))
				.findFirst().orElse(null);
	}

	private Date truncate(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
}