package org.cochabaen;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.cochabaen.model.Transaction;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

public class TransactionVerticle extends AbstractVerticle {
	MemoryTransactionBackEnd db;
	AtomicInteger sequence = new AtomicInteger(1);

	@Override
	public void start() {
		db = new MemoryTransactionBackEnd();
		EventBus eventBus = vertx.eventBus();

		eventBus.consumer("org.cochabaen.transactions/create")
				.handler(objectMessage -> {
					JsonObject payload = (JsonObject) objectMessage.body();
					System.out.println("payload: " + payload);
					Transaction t = payload.mapTo(Transaction.class);
					if (t.getReference() == null)
						t.setReference(Integer.toString(sequence.getAndIncrement()));
					else {
						// Duplicate reference check
						if (null != db.checkTransactionExists(t.getReference())) {
							objectMessage.reply("{\"code\":409,\"message\":\"Transaction "
									+ "reference is duplicated (transaction already exists)\"}");
							return;
						}
					}
					if (t.getDate() == null)
						t.setDate(new Date());
					if (t.getFee() == null)
						t.setFee(BigDecimal.ZERO);
					// Balance authorization check
					vertx.eventBus().request("org.cochabaen.transactions/authorize",
							Json.encode(t), reqAsyncResult -> {
								JsonObject response = new JsonObject(
										reqAsyncResult.result().body().toString());
								if (response.containsKey("ok")) {
									db.createTransaction(t);
									objectMessage.reply(Json.encode(t));
								} else {
									objectMessage.reply("{\"code\":422,\"message\":\"Transaction "
											+ "can't be created (entity is not processable)\"}");
								}
							});
				});
		eventBus.consumer("org.cochabaen.transactions/get")
				.handler(objectMessage -> {
					JsonObject payload = (JsonObject) objectMessage.body();
					System.out.println("payload: " + payload);
					objectMessage.reply(Json.encode(db.findByIban(
							payload.getString("account_iban"), payload.getString("sort"))));
				});
		eventBus.consumer("org.cochabaen.transactions/status")
				.handler(objectMessage -> {
					JsonObject payload = (JsonObject) objectMessage.body();
					System.out.println("payload: " + payload);
					objectMessage.reply(Json.encode(db.getTransactionStatus(
							payload.getString("reference"), payload.getString("channel"))));
				});
		System.out.println(this.getClass().getName() + " running...");
	}
}
