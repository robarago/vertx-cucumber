package org.cochabaen;

import java.util.HashMap;
import java.util.Map;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.validation.RequestParameter;
import io.vertx.ext.web.validation.RequestParameters;

public class MicroServiceVerticle extends AbstractVerticle {
	HttpServer server;

	private static Map<String, String> opSubject = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put("postTransaction", "org.cochabaen.transactions/create");
			put("getTransactions", "org.cochabaen.transactions/get");
			put("getTransactionStatus", "org.cochabaen.transactions/status");
		}
	};

	private void busRequest(String op, JsonObject req, RoutingContext rc,
			int sc) {
		vertx.eventBus().request(opSubject.get(op), req, reqAsyncResult -> {
			Object response = Json
					.decodeValue((String) reqAsyncResult.result().body());
			Integer status = response instanceof JsonObject
					&& ((JsonObject) response).containsKey("code")
							? ((JsonObject) response).getInteger("code") : sc;
			rc.response().setStatusCode(status)
					.putHeader("Content-Type", "application/json")
					.end(reqAsyncResult.result().body().toString());
		});
	}

	@Override
	public void start(Promise<Void> startPromise) {
		RouterBuilder.create(this.vertx, "cochabaen.yaml")
				.onSuccess(routerBuilder -> {
					// POST /transaction
					routerBuilder.operation("postTransaction").handler(rc -> {
						RequestParameters params = rc.get("parsedParameters");
						busRequest("postTransaction", params.body().getJsonObject(), rc,
								201);
					});
					// GET /transaction/findbyiban
					routerBuilder.operation("getTransactions").handler(routingContext -> {
						RequestParameters reqParams = routingContext
								.get("parsedParameters");
						JsonObject req = new JsonObject();
						req.put("account_iban",
								reqParams.queryParameter("account_iban").getString());
						RequestParameter sort = reqParams.queryParameter("sort");
						req.put("sort", sort == null ? "asc" : sort.getString());
						busRequest("getTransactions", req, routingContext, 200);
					});
					// GET /transaction/status
					routerBuilder.operation("getTransactionStatus")
							.handler(routingContext -> {
								RequestParameters reqParams = routingContext
										.get("parsedParameters");
								System.out.println("reqParams: " + reqParams);
								JsonObject req = reqParams.body().getJsonObject();
								busRequest("getTransactionStatus", req, routingContext, 200);
							});
					// Generate the router
					Router router = routerBuilder.createRouter();
					router.errorHandler(404, routingContext -> {
						JsonObject errorObject = new JsonObject().put("code", 404)
								.put("message", (routingContext.failure() != null)
										? routingContext.failure().getMessage() : "Not Found");
						routingContext.response().setStatusCode(404)
								.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
								.end(errorObject.encode());
					});
					router.errorHandler(400, routingContext -> {
						JsonObject errorObject = new JsonObject().put("code", 400).put(
								"message",
								(routingContext.failure() != null)
										? routingContext.failure().getMessage()
										: "Validation Exception");
						routingContext.response().setStatusCode(400)
								.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
								.end(errorObject.encode());
					});
					server = vertx.createHttpServer(
							new HttpServerOptions().setPort(3000).setHost("localhost"));
					server.requestHandler(router).listen().onSuccess(server -> System.out
							.println(this.getClass().getName() + " running..."));
					startPromise.complete();
				}).onFailure(startPromise::fail);
	}

	@Override
	public void stop() {
		this.server.close();
	}

}
