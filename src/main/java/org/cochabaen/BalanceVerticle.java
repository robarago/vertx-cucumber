package org.cochabaen;

import org.cochabaen.model.Transaction;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

public class BalanceVerticle extends AbstractVerticle {
	MemoryBalanceBackEnd db;

	@Override
	public void start() {
		db = new MemoryBalanceBackEnd();
		EventBus eventBus = vertx.eventBus();

		eventBus.consumer("org.cochabaen.transactions/authorize")
				.handler(objectMessage -> {
					JsonObject payload = new JsonObject(objectMessage.body().toString());
					System.out.println("payload: " + payload.toString());
					Transaction t = payload.mapTo(Transaction.class);
					if (db.authorize(t))
						objectMessage.reply("{\"ok\":true}");
					else
						objectMessage.reply("{\"ko\":false}");
				});
		System.out.println(this.getClass().getName() + " running...");
	}
}
