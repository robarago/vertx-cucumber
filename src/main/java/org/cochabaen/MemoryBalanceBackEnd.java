package org.cochabaen;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cochabaen.model.Transaction;

public class MemoryBalanceBackEnd {
	Map<String, BigDecimal> accounts;

	MemoryBalanceBackEnd() {
		accounts = new HashMap<String, BigDecimal>();
	}

	public boolean authorize(Transaction t) {
		BigDecimal net = t.getAmount().subtract(t.getFee());
		System.out.println("Net: " + net);
		BigDecimal balance = accounts.get(t.getAccountIban());
		System.out.println("Balance before: " + balance);

		if (balance == null)
			balance = new BigDecimal(0);
		BigDecimal newBalance = balance.add(net);
		if (newBalance.signum() == -1)
			return false;
		accounts.put(t.getAccountIban(), newBalance);
		System.out.println("Balance after: " + accounts.get(t.getAccountIban()));

		return true;
	}
}