# Aceptance tests

## Business rules (Create Transaction)

### CT-A
Given: A new transaction
When: The amount is positive
      And the transaction date is before today
      And with IBAN A
      And no fee
      And no reference
Then: The system returns the transaction
      And a new reference
      And a 0 fee

### CT-B
Given: A new transaction
When: The case A is executed
      The amount is negative but less than the amount of case A
      And the transaction date is after today
      And with IBAN A
      And no fee
      And no reference
Then: The system returns the transaction
      And a new reference
      And a 0 fee

### CT-C
Given: A new transaction
When: The case B is executed
      The amount is positive
      And the transaction date is equals to today
      And with IBAN A
      And no fee
      And no reference
Then: The system returns the transaction
      And a new reference
      And a 0 fee

## Business rules (Search Transactions)

### ST-A
Given: All created transactions
When: I search for transactions of IBAN A
      And no sort order is specified
Then: The system returns all the transactions of IBAN A
      And the order is ascending by amount

### ST-B
Given: All created transactions
When: I search for transactions of IBAN A
      And ascending sort order is specified
Then: The system returns all the transactions of IBAN A
      And the order is ascending by amount

### ST-C
Given: All created transactions
When: I search for transactions of IBAN A
      And descending sort order is specified
Then: The system returns all the transactions of IBAN A
      And the order is descending by amount

### ST-D
Given: All created transactions
When: I search for transactions of UNKNOWN IBAN
      And no sort order is specified
Then: The system returns an empty list

## Business rules (Transaction status)

### TS-A
Given: A transaction that is not stored in our system
When: I check the status from any channel
Then: The system returns the status 'INVALID'

### TS-B
Given: A transaction that is stored in our system
When: I check the status from CLIENT or ATM channel
      And the transaction date is before today
Then: The system returns the status 'SETTLED'
      And the amount substracting the fee

### TS-C
Given: A transaction that is stored in our system
When: I check the status from INTERNAL channel
      And the transaction date is before today
Then: The system returns the status 'SETTLED'
      And the amount
      And the fee

### TS-D
Given: A transaction that is stored in our system
When: I check the status from CLIENT or ATM channel
      And the transaction date is equals to today
Then: The system returns the status 'PENDING'
      And the amount substracting the fee

### TS-E
Given: A transaction that is stored in our system
When: I check the status from INTERNAL channel
      And the transaction date is equals to today
Then: The system returns the status 'PENDING'
      And the amount
      And the fee

### TS-F
Given: A transaction that is stored in our system
When: I check the status from CLIENT channel
      And the transaction date is greater than today
Then: The system returns the status 'FUTURE'
      And the amount substracting the fee

### TS-G
Given: A transaction that is stored in our system
When: I check the status from ATM channel
      And the transaction date is greater than today
Then: The system returns the status 'PENDING'
      And the amount substracting the fee

### TS-H
Given: A transaction that is stored in our system
When: I check the status from INTERNAL channel
      And the transaction date is greater than today
Then: The system returns the status 'FUTURE'
      And the amount
      And the fee

### TS-I
Given: A transaction that is not stored in our system
When: The amount is negative and greater than the balance
      And with IBAN A
Then: The system returns 422 Unprocessable Entity
 
### TS-J
Given: A transaction that is stored in our system
When: I want to create a transaction
      And with IBAN A
Then: The system returns 409 Conflict
